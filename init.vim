" Don't try to be vi compatible
set nocompatible

" Helps force plugins to load correctly when it is turned back on below
filetype off

" TODO: Load plugins here (pathogen or vundle)
call plug#begin('~/AppData/Local/nvim/plugged/autoload')
Plug 'tpope/vim-surround'
Plug 'scrooloose/syntastic'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-repeat'
Plug 'w0rp/ale'
Plug 'godlygeek/tabular'
Plug 'ervandew/supertab'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'yggdroot/indentline'
Plug 'sheerun/vim-polyglot'
Plug 'mbbill/undotree'
Plug 'itchyny/lightline.vim'
Plug 'yuttie/comfortable-motion.vim'
Plug 'kien/rainbow_parentheses.vim'
Plug 'ap/vim-css-color'
Plug 'thinca/vim-quickrun'
Plug 'skywind3000/asyncrun.vim'
Plug 'sainnhe/sonokai'
Plug 'nanotech/jellybeans.vim'
Plug 'sainnhe/vim-color-forest-night'
Plug 'morhetz/gruvbox'
Plug 'jiangmiao/auto-pairs'
Plug 'iloginow/vim-stylus'
Plug 'kchmck/vim-coffee-script'
Plug 'digitaltoad/vim-jade'
Plug 'dnitro/vim-pug-complete'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'w0rp/ale'
Plug 'morhetz/gruvbox'
Plug 'quramy/tsuquyomi'
Plug 'rust-lang/rust.vim'
Plug 'mbbill/undotree'
Plug 'junegunn/goyo.vim'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'junegunn/limelight.vim'
Plug 'sainnhe/vim-color-forest-night'
Plug 'skywind3000/vim-quickui'
Plug 'matze/vim-move'
Plug 'kqito/vim-easy-replace'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'sickill/vim-pasta'
Plug '907th/vim-auto-save'
Plug 'leafo/moonscript'
call plug#end()

" TODO: Pick a leader key
let mapleader = ","

command! -nargs=0 Prettier :CocCommand prettier.formatFile
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
nnoremap <leader>z :UndotreeToggle<CR>
nnoremap <leader>r :QuickRun<CR>
nnoremap <leader>e :CocCommand explorer<CR>
nnoremap <leader>t :CocCommand terminal.Toggle<CR>
nnoremap <leader>v :e $MYVIMRC<cr>
nnoremap <leader>f :Prettier<cr>
nnoremap <leader>u :UndotreeToggle<CR>
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

let g:comfortable_motion_scroll_down_key = "s"
let g:comfortable_motion_scroll_up_key = "w"
let g:move_key_modifier = 'm'

colorscheme everforest
" Turn on syntax highlighting
syntax on

" For plugins to load correctly
filetype plugin indent on

" Security
set modelines=0

" Show line numbers
set number

" Show file stats
set ruler

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

command! -nargs=0 Prettier :CocCommand prettier.formatFile
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
nnoremap <leader>z :UndotreeToggle<CR>
nnoremap <leader>r :QuickRun<CR>
nnoremap <leader>e :CocCommand explorer<CR>
nnoremap <leader>t :CocCommand terminal.Toggle<CR>
nnoremap <leader>v :e $MYVIMRC<cr>
nnoremap <leader>f :Prettier<cr>
nnoremap <leader>u :UndotreeToggle<CR>
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ }

let g:comfortable_motion_scroll_down_key = "s"
let g:comfortable_motion_scroll_up_key = "w"
let g:move_key_modifier = 'm'

colorscheme everforest
" Turn on syntax highlighting
syntax on

" For plugins to load correctly
filetype plugin indent on

" Security
set modelines=0

" Show line numbers
set number

" Show file stats
set ruler

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Whitespace
set wrap
set textwidth=79
set formatoptions=tcqrn1
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround

" Cursor motion
set scrolloff=3
set backspace=indent,eol,start
set matchpairs+=<:> " use % to jump between pairs
runtime! macros/matchit.vim

" Move up/down editor lines
nnoremap j gj
nnoremap k gk

" Allow hidden buffers
set hidden

" Rendering
set ttyfast

" Status bar
set laststatus=2

" Last line
set showmode
set showcmd

" Searching
nnoremap / /\v
vnoremap / /\v
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch
map <leader><space> :let @/=''<cr> " clear search

" Remap help key.
inoremap <F1> <ESC>:set invfullscreen<CR>a
nnoremap <F1> :set invfullscreen<CR>
vnoremap <F1> :set invfullscreen<CR>

" Textmate holdouts

" Formatting
map <leader>q gqip

" Visualize tabs and newlines
set listchars=tab:▸\ ,eol:¬
" Uncomment this to enable by default:
" set list " To enable by default
" Or use your leader key + l to toggle on/off
map <leader>l :set list!<CR> " Toggle tabs and EOL
